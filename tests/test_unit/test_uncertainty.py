"""unit test for uncertainties."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pytest


def test_uncertainty_vectors(comp_contribution, reaction_dict):
    """Test the different formats of the uncertainty matrix."""
    reactions = [reaction_dict["atpase"], reaction_dict["atpase"]]

    _, Ucov = comp_contribution.standard_dg_prime_multi(
        reactions, uncertainty_representation="cov"
    )
    _, Usqrt = comp_contribution.standard_dg_prime_multi(
        reactions, uncertainty_representation="sqrt"
    )
    _, Ufullrank = comp_contribution.standard_dg_prime_multi(
        reactions, uncertainty_representation="fullrank"
    )

    assert Ucov.shape == (2, 2)
    assert Usqrt.shape == (2, 669)
    assert Ufullrank.shape == (2, 1)

    assert Ucov.m_as("(kJ/mol)**2") == pytest.approx(0.0926, rel=1e-2)
    assert Ucov - Usqrt @ Usqrt.T == pytest.approx(0, abs=1e-3)
    assert Ucov - Ufullrank @ Ufullrank.T == pytest.approx(0, abs=1e-3)


def test_uncertainty_residuals(comp_contribution, reaction_dict):
    """Test that uncertainty due to generic compounds is handeled correctly."""
    reactions = [
        reaction_dict["nucleoside_diphosphase"],
        reaction_dict["adenylate_kinase"],
        reaction_dict["atpase"],
    ]

    _, Ucov = comp_contribution.standard_dg_multi(
        reactions, uncertainty_representation="cov"
    )
    _, Usqrt = comp_contribution.standard_dg_multi(
        reactions, uncertainty_representation="sqrt"
    )
    _, Ufullrank = comp_contribution.standard_dg_multi(
        reactions, uncertainty_representation="fullrank"
    )

    assert Ucov.shape == (3, 3)
    assert Usqrt.shape == (3, 671)
    assert Ufullrank.shape == (3, 3)

    assert Ucov - Usqrt @ Usqrt.T == pytest.approx(0, abs=1e-3)
    assert Ucov - Ufullrank @ Ufullrank.T == pytest.approx(0, abs=1e-3)


@pytest.mark.filterwarnings("ignore:Cannot calculate")
def test_adjust_dof(comp_contribution, reaction_dict):
    """Test the DOF adjustment."""
    reactions = [
        comp_contribution.parse_reaction_formula(
            "kegg:C00001 + kegg:C00002 = kegg:C00215"
        ),
        comp_contribution.parse_reaction_formula(
            "kegg:C00215 = kegg:C00008 + kegg:C00009"
        ),
    ]

    dg, _ = comp_contribution.standard_dg_prime_multi(
        reactions, minimize_norm=False
    )

    dg_adjusted, _ = comp_contribution.standard_dg_prime_multi(
        reactions, minimize_norm=True
    )

    # because the only degree of freedom is the unknown metabolite (C00215)
    # the sum of the two reaction energies must remain constant:
    assert (dg.sum() - dg_adjusted.sum()) == pytest.approx(0, abs=1e-3)
    assert (dg_adjusted[0] - dg_adjusted[1]) == pytest.approx(0, abs=1e-3)
